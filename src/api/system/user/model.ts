import {BaseEntity} from "@/api/base";

/**
 * @description: 用户导入
 * @param updateSupport 是否覆盖数据
 * @param file excel文件
 */
export interface UserImportParam {
  updateSupport: boolean;
  file: File | Blob;
}

/**
 * @description: 重置密码
 */
export interface ResetPwdParam {
  userId: string;
  password: string;
}

export interface Dept extends BaseEntity {
  deptId: number;
  parentId: number;
  parentName?: string;
  ancestors: string;
  deptName: string;
  orderNum: number;
  leader: string;
  phone?: string;
  email?: string;
  status: string;
}

export interface Role extends BaseEntity {
  roleId: string;
  roleName: string;
  roleKey: string;
  roleSort: number;
  dataScope: string;
  menuCheckStrictly?: boolean;
  deptCheckStrictly?: boolean;
  status: string;
  remark: string;
  flag: boolean;
  superAdmin: boolean;
}

export interface User extends BaseEntity {
  userId: number;
  tenantId: number;
  deptId: number;
  userName: string;
  nickName: string;
  userType: string;
  email: string;
  phonenumber: string;
  gender: string;
  avatar?: string;
  status: string;
  loginIp: string;
  loginDate: string;
  remark: string;
  dept: Dept;
  roles: Role[];
  roleIds?: string[];
  postIds?: number[];
  roleId: string;
}

export interface Post extends BaseEntity {
  postId: number;
  postCode: string;
  postName: string;
  postSort: number;
  status: string;
  remark: string;
}

/**
 * @description 用户信息
 * @param user 用户个人信息
 * @param roleIds 角色IDS 不传id为空
 * @param roles 所有的角色
 * @param postIds 岗位IDS 不传id为空
 * @param posts 所有的岗位
 */
export interface UserInfoResponse {
  user?: User;
  roleIds?: string[];
  roles: Role[];
  postIds?: number[];
  posts: Post[];
}

/**
 * @description: 部门树
 */
export interface DeptTree {
  id: number;
  parentId: number;
  label: string;
  weight: number;
  children?: DeptTree[];
}
