import { BaseEntity } from '@/api/base';

/**
 * @description 租户套餐
 * @param packageId id
 * @param packageName 名称
 * @param menuIds 菜单id  格式为[1,2,3] 返回为string 提交为数组
 * @param remark 备注
 * @param menuCheckStrictly 是否关联父节点
 * @param status 状态
 * @param version 乐观锁
 */
export interface TenantPackage extends BaseEntity {
  packageId: string;
  packageName: string;
  menuIds: string | number[];
  remark: string;
  menuCheckStrictly: boolean;
  status: string;
}
