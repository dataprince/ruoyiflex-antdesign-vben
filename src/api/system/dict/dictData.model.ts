import {BaseEntity} from "@/api/base";

export interface DictData extends BaseEntity {
  dictCode: number;
  dictSort: number;
  dictLabel: string;
  dictValue: string;
  dictType: string;
  cssClass: string;
  listClass: string;
  isDefault: string;
  status: string;
  default: boolean;
  remark: string;
}
