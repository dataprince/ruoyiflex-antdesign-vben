import {BaseEntity} from "@/api/base";

export interface DictType extends BaseEntity {
  dictId: number;
  dictName: string;
  dictType: string;
  status: string;
  remark: string;
}
