import { BaseEntity } from '@/api/base';
export interface Client extends BaseEntity {
  id: number;
  clientId: string;
  clientKey: string;
  clientSecret: string;
  grantTypeList: string[];
  grantType: string;
  deviceType: string;
  activeTimeout: number;
  timeout: number;
  status: string;
}
