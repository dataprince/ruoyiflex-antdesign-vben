import { BaseEntity } from '@/api/base';

export interface Tenant extends BaseEntity {
  tenantId: number;
  contactUserName: string;
  contactPhone: string;
  companyName: string;
  licenseNumber?: any;
  address?: string;
  domain?: string;
  intro: string;
  remark?: string;
  packageId?: string;
  expireTime?: string;
  accountCount: number;
  status: string;
}
