import {BaseEntity} from "@/api/base";

/**
 * @description: Post interface
 */
export interface Post extends BaseEntity {
  postId: number;
  postCode: string;
  postName: string;
  postSort: number;
  status: string;
  remark: string;
  createTime: string;
}
