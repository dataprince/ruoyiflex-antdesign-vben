import {BaseEntity} from "@/api/base";

export interface Config extends BaseEntity {
  configId: number;
  configName: string;
  configKey: string;
  configValue: string;
  configType: string;
  remark: string;
}
