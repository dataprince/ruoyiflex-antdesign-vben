import {BaseEntity} from "@/api/base";

export interface Dept extends BaseEntity {
  deptId: number;
  parentId: number;
  ancestors: string;
  deptName: string;
  orderNum: number;
  leader: string;
  phone: string;
  email: string;
  status: string;
  delFlag: string;
  parentName?: string;
  children?: Dept[];
  remark?: string;
}
