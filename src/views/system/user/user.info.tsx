import { DescItem } from '@/components/Description';
import { DictEnum } from '@/enums/dictEnum';
import { useRender } from '@/hooks/component/useRender';
import dayjs from 'dayjs';
import { Tag } from 'ant-design-vue';

function renderTags(list: string[]) {
  return (
    <div class="flex flex-row flex-wrap gap-0.5">
      {list.map((item) => (
        <Tag key={item}>{item}</Tag>
      ))}
    </div>
  );
}

const { renderDict } = useRender();
export const descSchema: DescItem[] = [
  {
    label: '用户ID',
    field: 'userId',
  },
  {
    label: '用户状态',
    field: 'status',
    render(value) {
      return renderDict(value, DictEnum.NORMAL_DISABLE);
    },
  },
  {
    label: '用户信息',
    field: 'nickName',
    render(_, data) {
      const { userName, nickName, dept = { deptName: '暂无部门信息' } } = data;
      return `${userName} / ${nickName} / ${dept.deptName}`;
    },
  },
  {
    label: '手机号',
    field: 'phonenumber',
    render(value) {
      return value || '未设置手机号码';
    },
  },
  {
    label: '邮箱',
    field: 'email',
    render(value) {
      return value || '未设置邮箱地址';
    },
  },
  {
    label: '岗位',
    field: 'postNames',
    render(value) {
      if (Array.isArray(value) && value.length === 0) {
        return '暂无信息';
      }
      return renderTags(value);
    },
  },
  {
    label: '权限',
    field: 'roleNames',
    render(value) {
      if (Array.isArray(value) && value.length === 0) {
        return '暂无信息';
      }
      return renderTags(value);
    },
  },
  {
    label: '创建时间',
    field: 'createTime',
  },
  {
    label: '上次登录IP',
    field: 'loginIp',
    render(value) {
      return value || <span class="text-orange-500">从未登录过</span>;
    },
  },
  {
    label: '上次登录时间',
    field: 'loginDate',
    render(value) {
      if (!value) {
        return <span class="text-orange-500">从未登录过</span>;
      }
      // 计算相差天数
      const diffDays = dayjs(new Date()).diff(dayjs(value), 'day');
      return (
        <div class="flex gap-2">
          {value}
          <Tag bordered={false} color="cyan">
            {diffDays}天前
          </Tag>
        </div>
      );
    },
  },
  {
    label: '备注',
    field: 'remark',
    render(value) {
      return value || '无';
    },
  },
];
