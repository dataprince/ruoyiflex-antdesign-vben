export enum RoleEnum {
  // 超级管理员角色
  SUPER_ADMIN = 'SuperAdminRole',

  // 租户管理员(小管理员)角色
  ADMIN = 'AdminRole',

  // tester
  TEST = 'test',
}
