import type { AppRouteModule } from '@/router/types';
import { LAYOUT } from '@/router/constant';

/**
 * 这里放本地路由  就是后台没有包含的
 * 默认登录后才能访问
 * 白名单路径: router/guard/permissionGuard.ts
 * 注意 component: LAYOUT 一定要有
 */
export const localRoutes: AppRouteModule[] = [
  {
    path: '/social-callback',
    name: 'socialCallback',
    component: () => import('@/views/auth/social-callback/index.vue'),
    meta: {
      title: '授权登录页',
    },
  },
  {
    component: LAYOUT,
    path: '/oss-config',
    name: 'OssConfig',
    meta: {
      title: 'OSS配置管理',
      hidden: true,
    },
    children: [
      {
        path: 'index',
        component: () => import('@/views/system/oss/OssConfig.vue'),
        name: 'OssConfigIndex',
        meta: {
          canTo: true,
          hidden: true,
          noTagsView: false,
          icon: 'ant-design:edit-outlined',
          title: 'OSS配置管理',
          dynamicLevel: 1,
        },
      },
    ],
  },
  {
    component: LAYOUT,
    path: '/gen',
    name: 'EditGenerate',
    meta: {
      title: '修改生成配置',
      hidden: true,
    },
    children: [
      {
        path: 'edit/:tableId',
        component: () => import('@/views/tool/gen/EditGenerate.vue'),
        name: 'EditGenerateIndex',
        meta: {
          canTo: true,
          hidden: true,
          noTagsView: false,
          icon: 'ant-design:edit-outlined',
          title: '修改生成配置',
          activeMenu: '/tool/gen',
          dynamicLevel: 3,
        },
      },
    ],
  },
  {
    component: LAYOUT,
    path: '/account',
    name: 'AcoountInfo',
    redirect: '/setting',
    meta: {
      hideMenu: true,
      title: '账号',
    },
    children: [
      {
        path: 'setting',
        name: 'AccountSettingPage',
        component: () => import('@/views/auth/profile/index.vue'),
        meta: {
          title: '个人设置',
        },
      },
    ],
  },
  {
    component: LAYOUT,
    path: '/assign-roles',
    name: 'AssignRoles',
    redirect: '/:roleId',
    meta: {
      hideMenu: true,
      title: '分配角色',
    },
    children: [
      {
        path: ':roleId',
        name: 'AssignRolesPage',
        component: () => import('@/views/system/role/AssignRoles/index.vue'),
        meta: {
          title: '分配角色',
        },
      },
    ],
  },
];
